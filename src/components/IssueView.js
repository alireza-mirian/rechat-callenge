import React from "react";
import usePromise from "react-use-promise";
import {getIssue} from "../githubApi";
import ReactMarkdown from "react-markdown";

export function IssueView({owner, repo, issueNumber}) {
	const [issue, error, loading] = usePromise(
		() => {
			if (owner && repo && issueNumber) {
				return getIssue({repoSlug: `${owner}/${repo}`, issueNumber});
			}
		},
		[owner, repo, issueNumber]);

	return <div>
		<h1>{repo} issue #{issueNumber}</h1>
		{issue && <Issue issue={issue} />}
	</div>;
}

export function Issue({issue}){
	return <>
		<h3>{issue.title}</h3>
		<ReactMarkdown source={issue.body}/>
	</>;
}