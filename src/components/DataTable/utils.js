import React, {useEffect} from "react";
import {Column} from "./Column";

export function index2Page(pageSize, index) {
	return Math.floor(index / pageSize);
}

export function getColumns(children) {
	return React.Children.map(children, child => {
		if (child.type === Column) {
			return child.props;
		} else {
			console.warn('Table can only host Column children. Passed: ', child.type);
			return null;
		}
	}).filter(i => i)
}

/**
 * scrollToIndex is broken since react@16.4.0.
 * [This PR](https://github.com/bvaughn/react-virtualized/pull/1288) seems to fix it,
 * but it's not merged yet. Until then we can use this workaround
 */
export function useScrollToIndexBugFix(tableRef, scrollToIndex) {
	useEffect(() => {
		if (scrollToIndex && tableRef.current) {
			const scrollTop = tableRef.current.getOffsetForRow({alignment: 'start', index: scrollToIndex});
			document.documentElement.scrollTop = scrollTop;
		}
	});
}