import React, {useRef} from "react";
import {AutoSizer, Column as ReactVirtualizedColumn, InfiniteLoader, Table, WindowScroller} from "react-virtualized";
import 'react-virtualized/styles.css';
import {getColumns} from "./utils";

/**
 * A data table component with baked-in support for virtual scroll and lazy loading data.
 * Total number of rows is passed, along with two required functions:
 * getItemAtIndex(index), loadRows({startIndex, stopIndex})
 * Based on scrolling viewport and scroll position only visible rows are loaded (via loadRows) and rendered.
 * @param rowCount
 * @param rowHeight
 * @param headerHeight
 * @param loadingRenderer
 * @param getItemAtIndex
 * @param isRowLoaded
 * @param loadRows: function to be used to load rows
 * @param scrollable: scrollable parent. Defaults to window
 * @param scrollToIndex
 * @param children: A list of Columns. same as {@link DataTable} children.
 * @constructor
 */
export function VirtualDataTable(
	{
		rowCount,
		rowHeight = 50,
		headerHeight = 50,
		loadingRenderer = () => 'loading',
		getItemAtIndex,
		isRowLoaded = (index) => getItemAtIndex(index) !== undefined,
		loadRows,
		scrollable,
		scrollToIndex,
		children
	}
) {
	const tableRef = useRef(null);
	// useScrollToIndexBugFix(tableRef, scrollToIndex);
	return <div className='table'>
		<WindowScroller scrollElement={scrollable || window}>
			{({height, isScrolling, registerChild: registerWindowScrollerChild, onChildScroll, scrollTop}) => {
				return (
					<InfiniteLoader isRowLoaded={isRowLoaded} loadMoreRows={loadRows} rowCount={rowCount}>
						{({registerChild, onRowsRendered}) => {
							registerChild(tableRef.current);
							return <AutoSizer disableHeight>
								{({width}) => {
									return <div ref={registerWindowScrollerChild}>
										<Table
											ref={tableRef}
											rowClassName={'row'}
											onRowsRendered={onRowsRendered}
											height={height}
											headerHeight={headerHeight || rowHeight}
											rowHeight={rowHeight}
											scrollToIndex={scrollToIndex}
											rowGetter={({index}) => {
												return getItemAtIndex(index) || {};
											}}

											autoHeight
											onScroll={onChildScroll}
											scrollTop={scrollTop}
											rowCount={rowCount}
											width={width}>
											{getColumns(children).map((column, index) =>
												<ReactVirtualizedColumn
													key={index} /* assuming columns are not reordered for now */
													width={column.width}
													className={'cell'}
													headerClassName={'cell'}
													label={column.label}
													headerRenderer={column.headerRenderer}
													cellRenderer={
														({cellData, columnData, dataKey, rowData, rowIndex}) => {
															if (isRowLoaded(rowIndex)) {
																return column.cellRenderer(rowData);
															}
															return loadingRenderer(rowIndex);
														}
													}
													dataKey={column.dataKey}/>)}
										</Table>
									</div>
								}}
							</AutoSizer>
						}
						}
					</InfiniteLoader>
				);
			}}
		</WindowScroller>
	</div>;
}