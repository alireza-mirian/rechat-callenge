export function Column({
												 label,
												 dataKey,
												 cellRenderer,
												 width,
												 headerRenderer,
											 }) {
	throw new Error('column should not be rendered directly');
}