import React from "react";
import 'react-virtualized/styles.css';
import {getColumns} from "./utils";

/**
 * data table component!
 * @param items
 * @param children
 * @returns {*}
 * @constructor
 */
export function DataTable({items, children}) {
	return <div>
		<table cellSpacing="0" cellPadding="0">
			<tbody>
			{
				items.map(item => {
					return <tr key={item.url}>
						{getColumns(children).map(column => <td>{column.cellRenderer(item)}</td>)}
					</tr>
				})
			}
			</tbody>
		</table>
	</div>;
}


