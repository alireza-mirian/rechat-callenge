import React, {useCallback, useRef} from "react";
import 'react-virtualized/styles.css';
import {index2Page} from "./utils";
import {VirtualDataTable} from "./VirtualDataTable";

/**
 * A more high-level component than {@link VirtualDataTable} which assumes data can be loaded in pages.
 *
 * @param isLoading
 * @param getItemAtIndex
 * @param loadPage
 * @param fromPage
 * @param toPage
 * @param pageSize
 * @param otherProps
 * @returns {*}
 * @constructor
 */
export function PaginatedVirtualDataTable(
	{
		isLoading, getItemAtIndex, loadPage, fromPage = 0, toPage, pageSize, ...otherProps
	}
) {

	const getItemAtRelativeIndex = useCallback((index) => {
		return getItemAtIndex(index + (fromPage * pageSize));
	}, [fromPage, getItemAtIndex, pageSize]);

	const isPageLoaded = useCallback(pageIndex => {
		let absoluteRowIndex = pageIndex * pageSize;
		while (absoluteRowIndex < (pageIndex + 1) * pageSize) {
			if (getItemAtIndex(absoluteRowIndex) === undefined) {
				return false;
			}
			absoluteRowIndex++;
		}
		return true;
	}, [getItemAtIndex, pageSize]);

	const loadingPages = useRef({});
	const loadRows = useCallback(async ({startIndex, stopIndex}) => {
		let page = index2Page(pageSize, startIndex + (fromPage * pageSize));
		const toPage = index2Page(pageSize, stopIndex + (fromPage * pageSize));
		while (page <= toPage) {
			if (!isPageLoaded(page) && !loadingPages.current[page]) {
				loadingPages.current[page] = true;
				await loadPage(page);
				delete loadingPages.current[page];
			}
			page++;
		}

	}, [pageSize, isLoading, isPageLoaded, fromPage]);

	return <VirtualDataTable
		{...otherProps}
		getItemAtIndex={getItemAtRelativeIndex}
		loadRows={loadRows}
		rowCount={(toPage - fromPage + 1) * pageSize} /* FIXME: last page size may vary from 1 upto pageSize *//>;
}