/**
 * A simple component to be used for showing dates
 * @return {string}
 */
export function DateOutput({date}) {
	return new Date(date).toDateString();
}