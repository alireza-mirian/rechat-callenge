import Chip from "@material-ui/core/Chip/index";
import Breadcrumbs from "@material-ui/lab/Breadcrumbs/index";
import React, {useCallback} from "react";
import {Link} from "react-router-relative-link";
import {useQueryParam} from "use-query-params/lib/index";
import {getIssues} from "../githubApi";
import {usePagedItems} from "../hooks/usePagedItems";
import {useInfiniteScroll} from "../use-infinite-scroll";
import {Column} from "./DataTable/Column";
import {PaginatedVirtualDataTable} from "./DataTable/PaginatedVirtualDataTable";
import {DateOutput} from "./DateOutput";


const DEFAULT_PAGE_SIZE = 30; // page size is assumed to be fixed.
const LABELS_CELL_WIDTH = 500;
const CELL_PADDING = 80;
const LABEL_HEIGHT = 36;

export function IssueList({owner, repo, pageSize = DEFAULT_PAGE_SIZE}) {
	const [items, isLoading, loadPage] = usePagedItems({
		fetchPage: (page, pageSize) => getIssues({repoSlug: `${owner}/${repo}`, page, pageSize}),
		pageSize,
	});
	const [page = 0, setPage] = useQueryParam('page', {
		decode: parseInt,
		encode: value => `${value}`
	});
	const getItemAtIndex = useCallback((index) => items[index], [items]);

	// const [fromPage, setFromPage] = useState(page);
	// const loadPrevious = useCallback(() => setFromPage(Math.max(0, fromPage - 1)), [fromPage]);
	const loadNext = useCallback(() => setPage(page + 1), [page]);

	useInfiniteScroll({
		loadNext,
		// loadPrevious,
		busy: false,
	});
	const getRowHeight = useCallback(({index}) => {
		const issue = getItemAtIndex(index);
		return issue ? heuristicallyGetIssueHeight(issue) : 50;
	}, [getItemAtIndex]);

	return <div>
		<Breadcrumbs aria-label="Breadcrumb">
			<a color="inherit" href={`https://github.com/${owner}`}>{owner}</a>
			<a color="inherit" href={`https://github.com/${owner}/${repo}`}>{repo}</a>
			<Link color="inherit" to={'.'}>Issues</Link>
		</Breadcrumbs>
		<br/>
		<PaginatedVirtualDataTable
			pageSize={pageSize}
			fromPage={0}
			toPage={page}
			loadPage={loadPage}
			rowHeight={getRowHeight}
			isLoading={isLoading}
			scrollToIndex={page * pageSize}
			loadingRenderer={() => <div className='cell-placeholder loading'/>}
			getItemAtIndex={getItemAtIndex}>
			<Column dataKey={'number'}
							width={250}
							label={'Issue Number'}
							cellRenderer={item => <Link to={`./${item.number}`}>#{item.number}</Link>}/>
			<Column dataKey={'login'}
							width={200}
							label={'Reporter'}
							cellRenderer={item => <>{item.user.login}</>}/>
			<Column dataKey={'login'}
							width={250}
							label={'Creation Date'}
							cellRenderer={item => <DateOutput date={item.created_at}/>}/>
			<Column dataKey={'labels'}
							width={LABELS_CELL_WIDTH}
							label={'Labels'}
							cellRenderer={item => <div style={{whiteSpace: 'normal'}}>{item.labels.map(label =>
								<Chip key={label.id}
											style={{backgroundColor: `#${label.color}`, margin: '2px 3px'}}
											label={label.name}/>)}</div>}/>
		</PaginatedVirtualDataTable>
	</div>
}


function heuristicallyGetIssueHeight(issue) {
	let rows = 1, currentRowWidth = 0;
	issue.labels.forEach((label) => {
		const labelWidth = heuristicallyGetLabelWidth(label);
		if (currentRowWidth + labelWidth > (LABELS_CELL_WIDTH - CELL_PADDING)) {
			currentRowWidth = 0;
			rows++;
		}
		currentRowWidth += labelWidth;
	});
	return rows * LABEL_HEIGHT + 10;
}


function heuristicallyGetLabelWidth(label) {
	return label.name.length * 7 + 30;
}