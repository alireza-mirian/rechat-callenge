import {useCallback, useState} from "react";

/**
 * Maintains a list of paginated items and its loading state.
 * Given a fetchPage function, returns a 3-tuple:
 * - items: An object in form of {index: item} which holds currently loaded items
 * - isLoading: A boolean indicating loading state
 * - loadPage: An async function to be called for loading page
 *
 * @param {Function} fetchPage: function in form of (page, pageSize) => Promise<items> to be used to fetch items
 * @param {number} pageSize
 * @returns {[*[], boolean, Function]}
 */
export function usePagedItems({fetchPage, pageSize}) {
	const [items, setItems] = useState({});
	const [isLoading, setLoading] = useState(false);
	const loadPage = useCallback(async (page) => {
		setLoading(true);
		try {
			const content = await fetchPage(page, pageSize);
			const newItems = content.reduce((soFar, item, index) => {
				soFar[(page * pageSize) + index] = item;
				return soFar;
			}, {});
			// Using an immutable collection may considerably improve performance when number of items is large
			setItems(items => ({...items, ...newItems}));
		} catch (e) {
			throw e;
		}
		setLoading(false);
	}, [pageSize]);
	return [items, isLoading, loadPage];
}