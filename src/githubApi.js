import axios from "axios";

export async function getIssues({repoSlug, page, pageSize = 20}) {
	const result = await axios.get(`https://api.github.com/repos/${repoSlug}/issues`, {
		params: {
			per_page: pageSize,
			page
		}
	});
	return result.data;
}

export async function getIssue({repoSlug, issueNumber}) {
	const result = await axios.get(`https://api.github.com/repos/${repoSlug}/issues/${issueNumber}`);
	return result.data;
}