import {useEffect} from "react";

/**
 *
 * @param loadNext
 * @param loadPrevious
 * @param reachedStart
 * @param reachedEnd
 * @param scrollOffset
 * @param busy
 * @param scrollable
 */
export function useInfiniteScroll({
																		loadNext,
																		loadPrevious,
																		reachedStart = false,
																		reachedEnd = false,
																		scrollOffset = 50,
																		busy = false,
																		scrollable = typeof window !== 'undefined' ? window : undefined,
																	}) {

	useEffect(() => {
		if (scrollable) {
			const scrollListener = (event) => {
				if (scrollable) {
					// TODO: improve logic to take scroll direction into account
					const htmlElement = scrollable === window ? document.documentElement : scrollable;
					const scrollTop = htmlElement.scrollTop;
					const scrollBottom = htmlElement.scrollHeight - htmlElement.clientHeight - scrollTop;
					const shouldLoadPrevious = scrollTop < scrollOffset;
					const shouldLoadNext = scrollBottom < scrollOffset;
					if (shouldLoadPrevious && loadPrevious && !reachedStart && !busy) {
						loadPrevious();
					}
					if (shouldLoadNext && loadNext && !reachedEnd && !busy) {
						loadNext();
					}
				}
			};
			scrollable.addEventListener('scroll', scrollListener);
			return () => {
				scrollable.removeEventListener('scroll', scrollListener);
			}
		}
	}, [scrollable, loadNext, loadPrevious, reachedStart, reachedEnd, busy])
}