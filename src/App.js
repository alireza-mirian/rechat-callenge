import CssBaseline from "@material-ui/core/CssBaseline";

import React, {Component} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {QueryParamProvider} from "use-query-params";
import './App.css';
import {IssueList} from "./components/IssueList";
import {IssueView} from "./components/IssueView";

class App extends Component {
	render() {
		return (
			<div className="container">
				<Router>
					<QueryParamProvider ReactRouterRoute={Route}>
						<CssBaseline/>
						<Switch>
							<Route path={'/:owner/:repo/issues'} exact
										 render={({match}) => <IssueList owner={match.params.owner} repo={match.params.repo}/>}
							/>
							<Route path={'/:owner/:repo/issues/:issueNumber'}
										 render={({match: {params}}) =>
											 <IssueView owner={params.owner} repo={params.repo} issueNumber={params.issueNumber}/>}/>
							<Redirect to={'/angular/angular/issues'} from={'/'}/>
						</Switch>
					</QueryParamProvider>
				</Router>
			</div>
		);
	}
}

export default App;
